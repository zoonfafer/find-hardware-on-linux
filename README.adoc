= find-hardware-on-linux

See: https://dortania.github.io/OpenCore-Install-Guide/find-hardware.html#finding-hardware-using-linux

== Motivation

To find out hardware info on Linux and to retrieve the results in JSON format.

== What it does

== Prerequisites

* Bash
** cat
** dmesg
** dmidecode
** grep
** lshw
** lspci

== Installation and usage

[source,console]
----
git clone https://gitlab.com/zoonfafer/find-hardware-on-linux && \
cd find-hardware-on-linux && \
bin/find-hardware-on-linux > hardware-info.json
----

== Usage

=== Options


`-h`::
  display usage info

`-V`::
  display version

`-d`::
  debug mode (implies verbose mode)

`-v`::
  verbose mode


== Licence

See link:LICENCE.adoc[`LICENCE.adoc`^].
