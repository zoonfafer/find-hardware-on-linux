#!/bin/bash
#
# https://dortania.github.io/OpenCore-Install-Guide/find-hardware.html#finding-hardware-using-linux
#
# Jeffrey Lau <who.else.at.jlau.tk>
# Fri Sep 25 07:46:20 HKT 2020
#

set -euo pipefail

readonly progname="${0##*/}"
readonly VERSION=0.0.1

declare VERBOSE=
declare DEBUG=

declare dmesg=""
declare lspci=""

trap "trap_err_exit" ERR INT

trap_err_exit() {
	err "Script failed: see failed command above"
	print_summary
	exit 1
}

err() {
	>&2 printf "\\e[1;31mError\\e[m: \\e[1m%s\\e[22m: %b\\e[m\\n" "${progname}" "$*"
}

warn() {
	>&2 printf "\\e[1;38;5;214mWarning\\e[m: \\e[1m%s\\e[22m: %b\\e[m\\n" "${progname}" "$*"
}

info() {
	if [[ -n "${VERBOSE}" ]] || [[ -n "${DEBUG}" ]]
	then
		printf "\\e[1;34mInfo\\e[m: \\e[1m%s\\e[22m: %b\\e[m\\n" "${progname}" "$*"
	fi
}

debug() {
	if [[ -n "${DEBUG}" ]]
	then
		printf "\\e[1;38;5;59mDebug\\e[m: \\e[23m%s\\e[2m: %b\\e[m\\n" "${progname}" "$*"
	fi
}


# https://stackoverflow.com/questions/6481005/how-to-obtain-the-number-of-cpus-cores-in-linux-from-the-command-line
determine_logical_cores() {
	case "${OSTYPE}" in
		darwin*)
			sysctl -n hw.ncpu ;;

		# bsd*)
		# linux*)
		# msys*)
		# solaris*)
		*)
			nproc --all ;;
	esac
}

show_version() {
	printf "%s v%s\\n" "${progname}" "${VERSION}"
}

show_usage() {
	printf "Usage: \\e[1m%s\\e[22m [OPTIONS] \\n" "${progname}"
	printf "
    \\e[1mOptions\\e[22m

	-h
	  display usage info

	-V
	  display version

	-d
	  debug mode (implies verbose mode)

	-v
	  verbose mode
"
}

print_summary() {
	info "Summary: ..."
}

# check to see if this file is being run or sourced from another script
is_sourced() {
	# https://unix.stackexchange.com/a/215279
	[[ "${#FUNCNAME[@]}" -ge 2 ]] \
		&& [[ "${FUNCNAME[0]}" = 'is_sourced' ]] \
		&& [[ "${FUNCNAME[1]}" = 'source' ]]
}

getoptions() {
	local opt

	while getopts "hVdnvpPw:" opt
	do
		case "${opt}" in
			h)
				show_usage
				exit ;;
			V)
				show_version
				exit ;;
			d)
				DEBUG=1 ;;
			v)
				VERBOSE=1 ;;
			*)
				err "Wrong usage.  Aborting."
				show_usage
				exit 1
				;;
		esac
	done
}

json_escape_string() {
	local input
	input="$(cat)"
	local nl=$'\n'
	local output="${input//${nl}/\\n}"
	local dq='"'
	output="${output//${dq}/\\${dq}}"
	local tab=$'\t'
	output="${output//${tab}/\\t}"
	echo "${output}"
}

grepit() {
	command grep "$@"
}

# memoize
dmesgit() {
	if [[ -z "${dmesg}" ]]
	then
		dmesg="$(command dmesg)"
	fi
	echo "${dmesg}"
}

# memoize
lspciit() {
	if [[ -z "${lspci}" ]]
	then
		lspci="$(command lspci)"
	fi
	echo "${lspci}"
}

dmidecodeit() {
	command dmidecode "$@"
}

lshwit() {
	command lshw "$@"
}

doit() {
	cat <<-EOF
	{
	"cpu": "$(</proc/cpuinfo grepit 'model name' | json_escape_string)",
	"gpu": "$(lspciit | grepit -i 'vga\|3d\|2d' | json_escape_string)",
	"chipset": "$(dmidecodeit -t baseboard | json_escape_string)",
	"keyboard_touchx": "$(dmesgit | grepit -i 'input' | json_escape_string)",
	"audio": "$(lspciit | grepit -i 'audio' | json_escape_string)",
	"network_basic": "$(lspciit | grepit -i 'network' | json_escape_string)",
	"network_indepth": "$(lshwit -class network | json_escape_string)",
	"drive": "$(lshwit -class disk -class storage | json_escape_string)"
	}
	EOF
}


check_usage() {
	if [[ "${OSTYPE}" != linux* ]]
	then
		err "Only Linux is supported.  Aborting."
		exit 1
	fi
}

main() {
	local OPTIND
	getoptions "$@"
	shift $((OPTIND-1))

	check_usage
	doit
}

if ! is_sourced
then
	main "$@"
fi

# vim:set ft=sh:syntax=bash
